#!/bin/sh
#Authur ola.apata@hotmail.com
#version 1.0.0 

default(){
 pink='\033[0;35m'
 NC='\033[0m' # No Color
 echo $gap
 echo "$pink                                                       "
 echo "  _____ _ _____           _       _                         "
 echo " |  __ (_)  __ \         | |     (_)                        "
 echo " | |__) || |__) |   _ ___| |__    _  ___                    "
 echo " |  ___/ |  ___/ | | / __| '_ \  | |/ _ \                   "
 echo " | |   | | |   | |_| \__ \ | | |_| | (_) |                  "
 echo " |_|   |_|_|    \__,_|___/_| |_(_)_|\___/                   "
 echo "                                                            "
 echo "$NC                                                         "
 echo "   A Raspberrypi tool & service Installation Manager.       "
 echo "______________________________________________________      "
 echo "                                                            "
 echo $gap

}

#variables
check_help_files="try 'pipush help' to see acceptable commands"
gap="                                                         "

error_empty_value() {
  echo "$red You are missing $1 $NC"
}
show_after_install_msg() {
  echo "Done, next type 'pipush start $1' to see any extra instructions on getting started"
}


show_before_install_msg() {
  echo "Once Installation is complete  type 'pipush start $1' to see instructions of getting started"
}


build_action() {
  python3  /usr/lib/pipush/pipush/build.py $1
  echo $gap
  show_after_install_msg $1
}  

build_action_not() {
  red='\033[0;31m'
  NC='\033[0m' # No Color
	echo "$red Installing $1 cancelled $NC"
  echo $gap
}                    

help(){
  default
  echo "install {user/package}       :   Installs the package "
  echo "dep                          :   List all dependencies "
  echo "search {-u user -p package}  :   search by user and or package"
  echo "version                      :   Displays current version of pipush"
  echo "update                       :   This will check and alert you of any pipush update"
  echo "create {user password script}:   This will create a package from the command line - registered user"
  echo "message {message}            :   Leave a message for the package owner"
  echo "star {user 1-5}              :   Leave feedback rating for package owner - registered user"
  echo "register {email}             :   Follow the instructions in your email"
  echo $gap
  echo "Report bugs @ pipush.com                                                                   "
}


build() {
  red='\033[0;31m'
  default
  if [ $1 ="" ]
      then
        error_empty_value " the Package Reference. $check_help_files" 
        exit 1
  fi
	echo "Package to Install : $1 "
  show_before_install_msg $1
  $gap
     while true; do
         read -p "Do you wish to install this package [Yes | No ]?" yn
         case $yn in
             [Yy]* ) build_action $1; break;;
             [Nn]* ) build_action_not; exit;;
             * ) echo "Please answer yes or no.";;
         esac
     done
}



list() {
  default
	echo "Packages available \n"
  python3  /usr/lib/pipush/pipush/list.py $1
  echo $gap
  show_after_install_msg $1 

}

update() {
  default
  curl -s pipush-pipush.rhcloud.com/install | sh
  echo "Update complete"
}

new_version() {
  echo "A newer version is available. Use 'pipush update'"
}

version() {
  sed -n 3p ./pipush
  new_version
}

case "$1" in
	build)
       build $2
       ;;
	list)
       list
       ;;
	version)
       version
       ;;
  dep)
       dep
       ;;
  help)
       help
       ;;
    *)
     default
     exit 2
esac

exit $?